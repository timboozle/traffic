<link rel="stylesheet" href="{{mix('css/app.css')}}">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<section class="hero">
  <div class="hero-body">
    <div class="container">
      <h1 class="title"><i class="fa fa-car" aria-hidden="true"></i> Reistijden</h1>
      <h2 class="subtitle">Thuis/werk</h2>
    </div>
  </div>
</section>
<div class="container">
        <table class="table is-bordered is-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th><i class="fa fa-home" aria-hidden="true"></i> Bestemming</th>
                    <th><i class="fa fa-arrows-h"></i> Afstand</th>
                    <th><i class="fa fa-calendar" aria-hidden="true"></i> Datum</th>
                    <th><i class="fa fa-car" aria-hidden="true"></i> Wegrijden om</th>
                    <th><i class="fa fa-clock-o" aria-hidden="true"></i> Tijd</th>
                </tr>
            </thead>
            <tbody>
                @foreach($trips as $trip)
                <tr>
                    <td>{{$trip->id}}</td>
                    <td>{{title_case($trip->direction)}}</td>
                    <td>{{distance($trip->distance)}}</td>
                    <td>{{$trip->created_at->formatLocalized('%A %d %B %Y')}}</td>
                    <td>{{$trip->created_at->format('H:i')}}</td>
                    <td>{{duration($trip->duration)}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @if($trips->isEmpty())
        <div class="content">
            <h3 class="has-text-centered">Nog geen tijden gevonden</h3>
        </div>
        @endif
</div>