<?php

use GuzzleHttp\Client;
use Carbon\Carbon;
use App\Trip;

Artisan::command('save-trip', function (Client $guzzle, Carbon $carbon) {

	// Google Maps
	$baseUrl      = 'https://maps.googleapis.com/maps/api/distancematrix/json';
	$apiKey       = 'AIzaSyDCp-O2QlH6laySKGIccQTDWhnq_qt-sZw';

	// Locations
	$home         = env('HOME_ADDRESS');
	$work         = env('WORK_ADDRESS');

	// Carbon Variables
	$now          = $carbon->now();
	$morningStart = $carbon->today()->hour(6);
	$morningEnd   = $carbon->today()->hour(9);
	$eveningStart = $carbon->today()->hour(15);
	$eveningEnd   = $carbon->today()->hour(19);
	$isMorning    = $now->between($morningStart, $morningEnd);
	$isEvening    = $now->between($eveningStart, $eveningEnd);

	if ($isMorning) {
		$direction   = 'work';
		$origin      = $home;
		$destination = $work;
	}
	elseif ($isEvening) {
		$direction   = 'home';
		$origin      = $work;
		$destination = $home;
	}
	else exit;

	$response = $guzzle->get($baseUrl, [
		'query' => [
			'origins'        => $origin,
			'destinations'   => $destination,
			'key'            => $apiKey,
			'departure_time' => $now->timestamp
		]
	]);

	if ($response->getStatusCode() != 200) exit;

	$data = json_decode($response->getBody());

	return Trip::create([
		'direction'  => $direction,
		'distance'   => $data->rows[0]->elements[0]->distance->value,
		'duration'   => $data->rows[0]->elements[0]->duration_in_traffic->value,
	]);

});