<?php

use Carbon\Carbon;
use Khill\Duration\Duration;

function distance($x) {
	return round($x / 1000, 1) . ' km';
}

function duration($x) {
	$duration = new Duration($x);
	return $duration->humanize();
}

Route::get('/', function () {
	return view('index', [
		'trips'  => App\Trip::all()
	]);
});